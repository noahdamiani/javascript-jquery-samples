$(function() {
		var injectHtml = '<ul class="circle-nav"><span class="images"><img data-url="0" src="/wp-content/themes/Divi/images/circle2.png" /><img data-url="1" src="/wp-content/themes/Divi/images/circle3.png" /><img data-url="2" src="/wp-content/themes/Divi/images/circle1.png" /></span><li class="planning" data-url="0"></li><li class="sales" data-url="1"></li><li class="hospitality" data-url="2"></li></ul>';
		
		var isDesktop = $(window).width() > 480;
		
		$(".circle-nav-wrap").html(injectHtml);

				var _intervalId;

			    function fadeTo(target) {
			    	if (target.is('img')) {
				    	target.hide();
				    	target.parent().append(target);
				        target.fadeIn(2000);
				    }
			    	else {
				    	target.hide();
				    	target.parent().append(target);
				    	target.prev().fadeOut('slow', function(){
					    	target.fadeIn(1000);
				    	});

				    } 

			    }
			    _intervalId = setInterval(function() {
			        if (isDesktop) fadeTo($('.images img:first'));
			        fadeTo($('.departments ul:first'));
			    }, 3000);
			    $('.circle-nav').on('click', 'li', function() {
			        clearInterval(_intervalId);
			
			        function clickData(num) {
			            var fadeInImg = $('img[data-url=' + num + ']');
			            var fadeInUl = $('ul[data-url=' + num + ']');
			            var last = fadeInImg.is(':last-child');
			            if (!last) {	
				           fadeTo(fadeInImg); 
				           fadeTo(fadeInUl);
			            }
			        }
			        clickData($(this).data('url'));
			    });	


	});
