$(document).ready(function(){

$('.loading').hide();
	// Customize twitter feed
	var hideTwitterAttempts = 0;
	function hideOrig() {
	    setTimeout( function() {
	        if ( $('[id*=twitter]').length ) {
	        $('[id*=twitter]').each( function(){
	            if ( $(this).width() == 220 ) {
	                $(this).width( 198 );
	            }
	            var ibody = $(this).contents().find( 'body' );
	            ibody.width( $(this).width() + 20 );
	
	            if ( ibody.find( '.timeline .stream .h-feed li.tweet' ).length ) {
	            //Hide Header/Footer
	            ibody.css('width', 'auto');
	            ibody.css('margin-right', 'auto');
	            ibody.css('margin-left', 'auto');
	            ibody.find( '.footer' ).hide();
	            ibody.find( '.timeline-header').hide();
	            ibody.find( '.timeline-footer').hide();
	            ibody.find('.timeline.var-chromeless').css('background','white');
	            //Set Timeline Padding
	            ibody.find('.timeline.var-chromeless').css('padding-left','15px');
	            ibody.find('.timeline.var-chromeless').css('padding-right','23px');
	            ibody.find('.timeline.var-chromeless').css('padding-top','15px');
	            ibody.find('.timeline.var-chromeless').css('padding-bottom','0');
	            //Set Timeline Margin
	            ibody.find('.timeline.var-chromeless').css('margin-left','15px');
	            ibody.find('.timeline.var-chromeless').css('margin-right','15px');
	            //Hide CNY Icon & Text
	            ibody.find('.var-narrow .tweet .header').hide();
	            //Hide Timestamp
	            ibody.find('.var-chromeless .permalink').hide();
	            //Set tweet padding
	            ibody.find( '.var-narrow.var-chromeless .tweet' ).css( 'padding-bottom', '0' );
	            //Tweet Border Bottom 
	            ibody.find('.tweet .e-entry-title').css('border-bottom-width','1px');
	            ibody.find('.tweet .e-entry-title').css('border-style','solid');
	            ibody.find('.tweet .e-entry-title').css('border-color','#d1d1d1');
	            ibody.find('.tweet .e-entry-title').css('padding-bottom','25px');
	            ibody.find('li:last-child .e-entry-content .e-entry-title').css('padding-bottom','14px');
	            //Load once styles are present
	            $('.loading').addClass('loaded');
		            if ($('.loaded').length) {
						$('.loading').fadeIn('slow');
						$('.whats-new').fadeIn('slow');
					}
	            }
	            else {
	                $(this).hide();
	            }
	        });
	        }
	        hideTwitterAttempts++;
	        if ( hideTwitterAttempts < 3 ) {
	            hideOrig();
	        }
	    }, 1500);
	}

	hideOrig();
});