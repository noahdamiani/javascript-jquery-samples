$(function() {
    var classes = [];
    var li = $('ul li'),
        i = 0;
    var index = 0,
        length = li.length;
    
    li.each(function() {
        i += 1;
        var activeBG = "active-" + i;
        classes.push(activeBG);
        $(this).addClass(activeBG);
        
        if (i == li.length) {
            removeClasses();
        }
    });

    function removeClasses() {
        setTimeout(function() {
            $("ul li:eq(" + index + ")").removeClass(classes.join(' '))
            index++;
            if (index == length) index = 0;
            removeClasses();
        }, 500);
    }
});