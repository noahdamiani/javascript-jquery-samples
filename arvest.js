var isAndroidBrowser = /Android/i.test(navigator.userAgent) && navigator.userAgent.indexOf('Chrome') == -1;

if(!isAndroidBrowser) {
	$(window).scroll(function () {
		if ($(document).scrollTop() > 250) {
			$('.mobile-menu').addClass('fixed');
		} else {
			$('.mobile-menu').removeClass('fixed'); 
		}
	});
}

$(function() {

	if (screen.width > 500) {
		$(".tel").removeAttr("href");
	}

	// TARGET KINDLE
	var ua = navigator.userAgent;
	var isKindle = /Kindle/i.test(ua) || /Silk/i.test(ua) || /KFTT/i.test(ua) || /KFOT/i.test(ua) || /KFJWA/i.test(ua) || /KFJWI/i.test(ua) || /KFSOWI/i.test(ua) || /KFTHWA/i.test(ua) || /KFTHWI/i.test(ua) || /KFAPWA/i.test(ua) || /KFAPWI/i.test(ua);
	// TABLET NAV VARIATIONS 
	if(/iPhone|iPad|iPod/i.test(navigator.userAgent) ) {
		$(window).scroll(function () {
			if ($(document).scrollTop() > 250) {
				$('.mobile-menu').addClass('fixed');
			} else {
				$('.mobile-menu').removeClass('fixed'); 
			}
		});
		$('.top-navigation a').on('touchend',function(e){
    	 	e.preventDefault(); 
    	 });
    	 
		$('.menu-item a').on('touchend',function(e){
		    e.stopPropagation();
		    $(this).next().fadeOut('fast');
		    if (!$(this).hasClass('active')) {   
				$('.active').removeClass('active').next().fadeOut('fast');
			} 
			if ($(this).hasClass('active')) {
				$(this).addClass('inactive');
				$('.inactive').removeClass('active');
				return;  
			} else {		
				$(this).removeClass('inactive');
				$(this).addClass('active').next().fadeIn('fast');
				
				if ($('.my-accounts-login .sub-nav').is(':visible')) {
					$('.my-accounts-login .sub-nav').fadeOut('fast');
				}	
			}
		});
		
		$('.menu-item .sub-nav').on('touchend', function(e) {
		    e.stopPropagation();
		    e.preventDefault(); 
		});
		$(document).on('touchend', function(){
		     $('.menu-item .sub-nav').fadeOut('fast');
		     $('.menu-item a').removeClass('active');
		});
	}

	if(/Android/i.test(navigator.userAgent) || isKindle) {
		$('.menu-item .sub-nav').on('touchend', function(e) {
		    e.stopPropagation();
		    e.preventDefault(); 
		});
		
		$(document).on('touchend', function(){
		     $('.menu-item .sub-nav').fadeOut('fast');
		     $('.menu-item a').removeClass('active');
		});
		$('.top-navigation a').click(function(e){
    	 	e.preventDefault(); 
		 });
     
		$('.menu-item a').on('touchend', function(e){ 
		    e.stopPropagation();
		    
		    $(this).next().fadeOut('fast');
		   
		    if (!$(this).hasClass('active')) {   
				$('.active').removeClass('active').next().fadeOut('fast');
			} 
			if ($(this).hasClass('active')) {
				$(this).addClass('inactive');
				$('.inactive').removeClass('active');
				return;  
			} else {		
				$(this).removeClass('inactive');
				$(this).addClass('active').next().fadeIn('fast');
				
				if ($('.my-accounts-login .sub-nav').is(':visible')) {
					$('.my-accounts-login .sub-nav').fadeOut('fast');
				}
					
			}
			
		});
		
		$(document).on('click touchend', function(e) {				
					var homeLogin = $(".my-accounts-login form");
					if (!homeLogin.is(e.target)&& homeLogin.has(e.target).length === 0){
						$('.my-accounts-login .sub-nav').fadeOut('fast');
		
					}	
				});
		
		$('.my-accounts-login .login-btn').on('touchend', function() {
					$('.my-accounts-login .sub-nav').fadeToggle('fast');
			});
		
		} else {
			$('.top-navigation a').click(function(e){
		    	 e.preventDefault(); 
		     });
		     
			$('.menu-item a').click(function(e){ 
			    e.stopPropagation();
			    $(this).next().fadeOut('fast');
			    if (!$(this).hasClass('active')) {   
					$('.active').removeClass('active').next().fadeOut('fast');
				} 
				if ($(this).hasClass('active')) {
					$(this).addClass('inactive');
					$('.inactive').removeClass('active');
					return;  
				} else {		
					$(this).removeClass('inactive');
					$(this).addClass('active').next().fadeIn('fast'); 	
				}
				
				if ($('.my-accounts-login .sub-nav').is(':visible')) {
					$('.my-accounts-login .sub-nav').fadeOut('fast');
				}
				
			});
			
			$('.menu-item .sub-nav').click(function(e){
			    e.stopPropagation();
			});
			
			$(document).click(function(){
			     $('.menu-item .sub-nav').fadeOut('fast');
			     $('.menu-item a').removeClass('active');
			});
			
			$(document).on('touchend', function(){
		     $('.menu-item .sub-nav').fadeOut('fast');
		     $('.menu-item a').removeClass('active');
			 });
		
			$(document).on('click touchend', function(e) {				
					var homeLogin = $(".my-accounts-login form");
					if (!homeLogin.is(e.target)&& homeLogin.has(e.target).length === 0){
						$('.my-accounts-login .sub-nav').fadeOut('fast');
		
					}	
				});
				
			$('.my-accounts-login .login-btn').on('click', function() {
					$('.my-accounts-login .sub-nav').fadeToggle('fast');
			});
	}
	


	// LEFT MENU 
	$(".expand-menu").click(function(e) {
		e.stopPropagation();
		$('.expand-menu').hide('fast');
		$('.left-menu-inner').addClass('block');
		$('.close-menu').show('fast');
		return false;
	});
	$(".close-menu, .left-menu-inner a").click(function(e) {
		e.stopPropagation();
		$('.close-menu').hide('fast');
		$('.left-menu-inner').removeClass('block');
		$('.expand-menu').show('fast');
		return false;
	});		
	$('.arvest-slider').cycle({
		slides: 'div',
		fx: 'scrollHorz',
		timeout: 0,
		pager: '.icon-button-row',
		pagerTemplate: ''
	});
	
	$('.tagline-overlay').cycle({
		slides: 'div',
		fx: 'scrollHorz',
		timeout: 0,
		pager: '.icon-button-row',
		pagerTemplate: ''
	});
	//IE7 z-index fix 
	var zIndexNumber = 1000;
	$(".ie7 .row").each(function() {
           $(this).css('zIndex', zIndexNumber);
           zIndexNumber -= 10;
    });
 


});