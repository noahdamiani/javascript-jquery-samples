$(document).ready(function() {
	//Toggle Subnavigation
	$("a.nav-toggler").click(function() {
        $("#more-menu").slideToggle();
    });
    //Toggle User Menu
    $("li.nv-login").click(function() {
        $("#nv-account-menu").slideToggle();
    });
	// Unwrap subnav elements for Mobile Viewing
	$( "#phone-nav-link" ).click(function() {
	    var listItem = $("li.more li").detach();
	    $("li.more").remove();
	    listItem.appendTo("#mobile-sub-nav");
    });

	$(".nv-search a").click(function() {
		$("#nv-search-menu").slideToggle();
	});
});
